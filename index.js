var util = require('util');
var vm = require('vm');
var Hose = require('flowr-hose');
var trycatch = require('trycatch');

var hose = new Hose({
  name: 'ops',
  port: process.env.PORT || 0,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

console.log("FLOWR.OPS is running...");

hose.process('transform', process.env.CONCURRENCY || 1, function(job, done){
	// check for needed params!!
	job.log('transform: starting transform operation');
	transform(job.data, function(err, output){
		job.log('transform: operation finished');
    job.log(JSON.stringify(output));
		done(err, output);
	});
});

function transform(data, cb){
  console.log(data)
	var orig = data.input;
	var fn = data.transform;

	var sandbox = {
		input : orig
	};

  trycatch(function() {
    vm.runInNewContext(fn+' var output = transform(input)', sandbox);
  	cb(null, sandbox.output);
  }, function(err) {
    cb(new Error(err));
  });
}
