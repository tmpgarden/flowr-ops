FROM node:alpine
RUN apk add -U git
WORKDIR /opt/app
COPY package.json /opt/app/
RUN npm install
ADD index.js /opt/app/
ENTRYPOINT ["npm", "start"]
